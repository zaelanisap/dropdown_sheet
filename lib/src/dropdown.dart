import 'package:flutter/material.dart';

class DropdownSheet<T> extends StatefulWidget {
  ///Display on header of [BottomSheet]
  final String title;
  final String? hintText;
  final bool isDense;
  final List<SimpleSheetItem<T>> items;
  final T? selectedItem;

  ///Display text when [selectedItem] is not in value of [items]
  final String? initialText;
  final EdgeInsetsGeometry? contentPadding;
  final Widget? prefixIcon;
  final Color? selectedColor;
  final Color? backgroundColor;
  final InputBorder? border;
  final InputBorder? enabledBorder;
  final InputBorder? focusedBorder;
  final InputBorder? errorBorder;
  final TextStyle? textStyle;
  final void Function(T? value) onSelected;
  final String? Function(String?)? validator;
  const DropdownSheet({
    Key? key,
    required this.title,
    this.hintText,
    this.initialText,
    required this.items,
    required this.selectedItem,
    required this.onSelected,
    this.contentPadding,
    this.isDense = false,
    this.selectedColor,
    this.backgroundColor,
    this.validator,
    this.prefixIcon,
    this.textStyle,
    this.border,
    this.enabledBorder,
    this.focusedBorder,
    this.errorBorder,
  }) : super(key: key);

  @override
  State<DropdownSheet<T>> createState() => _DropdownSheetState<T>();
}

class _DropdownSheetState<T> extends State<DropdownSheet<T>> {
  late TextEditingController controller;
  SimpleSheetItem<T> get _item => widget.items.firstWhere(
        (e) => e.value == widget.selectedItem,
        orElse: () => SimpleSheetItem<T>(title: widget.initialText ?? ''),
      );

  @override
  void initState() {
    super.initState();

    controller = TextEditingController(text: _item.title);
  }

  @override
  void didUpdateWidget(covariant DropdownSheet<T> oldWidget) {
    if (oldWidget.selectedItem != widget.selectedItem ||
        oldWidget.items != widget.items) {
      controller = TextEditingController()..text = _item.title;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      autofocus: false,
      readOnly: true,
      textAlignVertical: TextAlignVertical.center,
      style: widget.textStyle,
      validator: widget.validator,
      onTap: () {
        showPopupItemsSheet<T>(context,
            selectedItem: widget.selectedItem,
            hintText: widget.title,
            items: widget.items, onSelected: (value) {
          widget.onSelected(value.value);
          controller.text = value.title;
        }, selectedColor: widget.selectedColor);
      },
      decoration: InputDecoration(
        prefixIcon: widget.prefixIcon,
        suffixIcon: Icon(Icons.keyboard_arrow_down),
        hintText: widget.hintText ?? widget.title,
        hintStyle: widget.textStyle,
        contentPadding: widget.contentPadding,
        isDense: widget.isDense,
        filled: true,
        fillColor: widget.backgroundColor,
        enabled: true,
        border: widget.border,
        enabledBorder: widget.enabledBorder,
        focusedBorder: widget.focusedBorder,
        errorBorder: widget.errorBorder,
      ).applyDefaults(Theme.of(context).inputDecorationTheme),
    );
  }
}

void showPopupItemsSheet<T>(BuildContext context,
    {required String hintText,
    required List<SimpleSheetItem<T>> items,
    required T? selectedItem,
    required void Function(SimpleSheetItem<T> value) onSelected,
    Color? selectedColor}) {
  if (items.isNotEmpty)
    showModalBottomSheet(
      context: context,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(8)),
      ),
      enableDrag: true,
      isDismissible: true,
      isScrollControlled: true,
      builder: (context) {
        return Container(
          height: MediaQuery.of(context).size.height / 2,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                height: 6,
                width: 48,
                decoration: BoxDecoration(
                    color: Colors.grey.shade400,
                    borderRadius: BorderRadius.circular(30)),
              ),
              ListTile(
                dense: true,
                title: Text(hintText,
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
              ),
              Expanded(
                child: items.isNotEmpty
                    ? Scrollbar(
                        child: ListView.separated(
                          primary: false,
                          itemCount: items.length,
                          separatorBuilder: (context, index) =>
                              Divider(height: 0, color: Colors.black26),
                          itemBuilder: (context, index) {
                            var item = items[index];
                            return RadioListTile<T?>(
                              groupValue: selectedItem,
                              value: item.value,
                              controlAffinity: ListTileControlAffinity.trailing,
                              tileColor: item.value == selectedItem
                                  ? selectedColor ??
                                      Theme.of(context).selectedRowColor
                                  : null,
                              title: Text(
                                item.title,
                                style: TextStyle(
                                    fontWeight: item.value == selectedItem
                                        ? FontWeight.w600
                                        : FontWeight.normal),
                              ),
                              onChanged: (value) {
                                onSelected.call(item);
                                Navigator.of(context).pop();
                              },
                            );
                          },
                        ),
                      )
                    : Center(child: Text('Tidak ada data')),
              )
            ],
          ),
        );
      },
    );
}

class SimpleSheetItem<T> {
  final String title;
  final T? value;
  SimpleSheetItem({
    required this.title,
    this.value,
  });

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SimpleSheetItem<T> &&
        other.title == title &&
        other.value == value;
  }

  @override
  int get hashCode => title.hashCode ^ value.hashCode;
}
